package com.sigad.autenticador.security;

import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {
	
		// EXPIRATION_TIME = 10 dias
		static final long EXPIRATION_TIME = 860_000_000;
		static final String SECRET = "MySecret";
		static final String TOKEN_PREFIX = "Bearer";
		static final String HEADER_AUTH = "Authorization";
		static final String HEADER_ORIGIN = "Access-Control-Allow-Origin";
		static final String HEADER_CRED = "Access-Control-Allow-Credentials";
		static final String HEADER_HEADERS = "Access-Control-Allow-Headers";
		static final String HEADER_METH = "Access-Control-Allow-Methods";
		static final String HEADER_EXP = "Access-Control-Expose-Headers";
		
		static void addAuthentication(HttpServletResponse response, String username) {
			String JWT = Jwts.builder()
					.setSubject(username)
					.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
			response.addHeader(HEADER_AUTH, TOKEN_PREFIX + " " + JWT);
			response.addHeader(HEADER_ORIGIN,"*"); 
			response.addHeader(HEADER_CRED, "true");
			response.addHeader(HEADER_HEADERS, "Content-Type");
			response.addHeader(HEADER_METH,"GET,HEAD,OPTIONS,POST,PUT");
			response.addHeader(HEADER_EXP,"Authorization");
		}
		
		static Authentication getAuthentication(HttpServletRequest request) {
			String token = request.getHeader(HEADER_AUTH);
			
			if (token != null) {
				// faz parse do token
				String user = Jwts.parser()
						.setSigningKey(SECRET)
						.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
						.getBody()
						.getSubject();
				
				if (user != null) {
					return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
				}
			}
			return null;
		}
}
